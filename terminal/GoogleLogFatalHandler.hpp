#ifndef __GOOGLE_LOG_FATAL_HANDLER_H__
#define __GOOGLE_LOG_FATAL_HANDLER_H__

namespace et {
class GoogleLogFatalHandler {
 public:
  static void handle();
};
}  // namespace et

#endif  // __GOOGLE_LOG_FATAL_HANDLER_H__
